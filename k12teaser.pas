program k12_intro;
{$librarypath '../blibs'}
uses atari, b_system, objects, rmt;

const 
{$i const.inc}
{$i 'bmp/bitmaps.inc'}
{$r 'bmp/bitmaps.rc'}
{$r introresources.rc}

label slide1,slide2,slide3,slide4,slide5,slide6,slide7,slide8,slide9;
label slide10,slide11,slide12,slide13,slide14,slide15,slide16,slide17,slide18,slide19;
label slide20,slide21,slide22,slide23,slide24,slide25,slide26;

type TStripe = record
    bitmap:cardinal;
    size:word;
    height:byte;
    top:byte;
    bottom:byte;
end;

var 
    xmem: TMemoryStream;
    gfx: array [0..0] of byte absolute GFX_MEM;
    vmem: array [0..0] of byte absolute VIDEO_RAM_ADDRESS;
    buffer4k: array [0..0] of byte absolute BUFFER;
    lineOffset: array [0..SCREEN_HEIGHT-1] of word;
    linesBrightness: array [0..SCREEN_HEIGHT-1] of byte;
    fadeDelay:byte;
    fxDelay:byte=1;
    topStripe, stripe1, stripe2: TStripe;
    i:byte;
    msx: TRMT;    
    msx_tick: byte = 0;
    msx_line: byte = 0;
    msx_pattern: byte = 0;
    sirenptr: byte = $ff;

function MakeStripe(bitmap:cardinal;size:word;top:byte):TStripe;
begin
    result.bitmap:=bitmap;
    result.size:=size;
    result.height:=size div 40;
    result.top:=top;
    result.bottom:=top + result.height - 1;
end;
    
procedure LoadPic(stripe:TStripe);
begin
    xmem.Position:=stripe.bitmap;
    xmem.ReadBuffer(gfx[lineOffset[stripe.top]],stripe.size);
end;    

procedure BuffPic(stripe:TStripe; offset:word);
begin
    xmem.Position:=stripe.bitmap;
    xmem.ReadBuffer(buffer4k[offset],stripe.size);
end;    

procedure LoadBuf(stripe:TStripe; offset:word);
begin
    move(buffer4k[offset], @gfx[lineOffset[stripe.top]], stripe.size);
end;    

procedure dli;assembler;interrupt;
asm {
    pha:tya:pha
    sta ATARI.wsync
    ldy #0
@   lda ADR.linesBrightness,y
    sta ATARI.colpf1
    sta ATARI.wsync
    iny
    cpy #200
    bne @-
    mva #8 ATARI.colpf1
    pla:tay:pla
};
end;

procedure Vbl; interrupt;
begin
    asm {
        sta regA
        stx regX
        sty regY
    };

    msx.Play;
    msx.Play;
    msx.Play;

asm {
        inc msx_tick
        lda msx_tick
        cmp #24
        bne @+
        mva #0 msx_tick
        inc msx_line
        lda msx_line
        cmp #16
        bne @+
        mva #0 msx_line
        inc msx_pattern
@
        mva $02C5 $D017
        mva $02C6 $D018
        mva $02C7 $D019
        mva $02C8 $D01A

        lda #0
regA    equ *-1
        ldx #0
regX    equ *-1
        ldy #0
regY    equ *-1
        rti
};
end;

procedure VblSiren; interrupt;
begin
    asm {
        sta regA
        stx regX
        sty regY
        
        ldy sirenptr;
        cpy #$ff
        beq @+

        lda ($fe),y
        sta $D201
        sta $D203
        sta $D205
        sta $D207
        sta $D211
        sta $D213
        sta $D215
        sta $D217
        lda ($fc),y
        sta $D200
        sub #1
        sta $D210
        sub #1
        sta $D202
        sub #1
        sta $D212
        sub #1
        sta $D204
        sub #2
        sta $D214
        sub #2
        sta $D206
        sub #2
        sta $D216

        inc sirenptr;
        jmp done

@       lda #0
        sta $d201
        sta $d203
        sta $d205
        sta $d207
        
done
        lda #0
regA    equ *-1
        ldx #0
regX    equ *-1
        ldy #0
regY    equ *-1
        rti
};
end;

procedure clearGFX;
begin
    FillByte(gfx[0],40*SCREEN_HEIGHT,0);
end;

procedure setBrightness(brightness:byte);
var i:byte;
begin
    WaitFrames(fadeDelay);
    for i:=0 to SCREEN_HEIGHT-1 do linesBrightness[i] := brightness;
end;

procedure brightenTo(brightness:byte);
var i:byte;
begin
    WaitFrames(fadeDelay);
    for i:=0 to SCREEN_HEIGHT-1 do if linesBrightness[i]<brightness then linesBrightness[i] := brightness;
end;

procedure darkenTo(brightness:byte);
var i:byte;
begin
    WaitFrames(fadeDelay);
    for i:=0 to SCREEN_HEIGHT-1 do if linesBrightness[i]>brightness then linesBrightness[i] := brightness;
end;

procedure setBrightnessRange(stripe:TStripe; brightness:byte);
var i:byte;
begin
    WaitFrames(fadeDelay);
    for i:=stripe.top to stripe.bottom do linesBrightness[i] := brightness;
end;

procedure interlaceRange(stripe:TStripe; brightness, darkness:byte);
var i:byte;
begin
    WaitFrames(fxDelay);
    for i:=stripe.top to stripe.bottom do begin
        if i and 1 = 1 then linesBrightness[i]:=brightness
        else linesBrightness[i]:=darkness;
    end;
end;

procedure slideDown(stripe:TStripe; brightness, step:byte);
var line:byte;
    s:shortInt;
begin
    line:=stripe.top;
    s:=step;
    repeat 
        if s<1 then begin
            WaitFrames(fxDelay);
            s:=step;
        end;
        linesBrightness[line]:=brightness;
        inc(line);
        dec(s);
    until line > stripe.bottom;
end;

procedure slideUpDown(stripe:TStripe; brightness, step:byte);
var line,max:byte;
    s:shortInt;
begin
    line:=stripe.top;
    max:=stripe.bottom;
    s:=step;
    repeat 
        if s<1 then begin
            WaitFrames(fxDelay);
            s:=step;
        end;
        linesBrightness[line]:=brightness;
        linesBrightness[max]:=brightness;
        inc(line);
        dec(max);
        dec(s);
    until line > max;
end;

procedure slideLeft(stripe:TStripe; brightness:byte);
var offset,y:byte;
    src,dst:word;
begin
    move(gfx[lineOffset[stripe.top]],buffer4k[0],lineOffset[stripe.height]);
    fillByte(gfx[lineOffset[stripe.top]],lineOffset[stripe.height],0);
    setBrightnessRange(stripe, brightness);
    
    for offset:=0 to 39 do begin
        WaitFrames(fxDelay);
        src := offset;
        dst := lineOffset[stripe.top] + offset;
        for y := 0 to stripe.height - 1 do begin
            gfx[dst] := buffer4k[src];
            inc(dst,40);
            inc(src,40);
        end;
    end;
end;

procedure stripeIn(stripe:TStripe; brightness, step:byte);
var y,offset:byte;
    linesOn: byte;
begin
    linesOn:=0;
    repeat 
        y:=random(byte(stripe.height));
        offset:=stripe.top+y;
        if linesBrightness[offset]<brightness then begin
            WaitFrames(fxDelay);
            linesBrightness[offset]:=linesBrightness[offset]+step;
            if linesBrightness[offset]>=brightness then begin
                linesBrightness[offset]:=brightness;
                inc(linesOn);
            end;
        end;
    until linesOn = stripe.height;
end;

procedure stripeOut(stripe:TStripe);
var y,offset:byte;
    linesOn: byte;
begin
    linesOn:=0;
    repeat 
        y:=random(byte(stripe.height));
        offset:=stripe.top+y;
        if linesBrightness[offset]>0 then begin
            WaitFrames(fxDelay);
            linesBrightness[offset]:=0;
            inc(linesOn);
        end;
    until linesOn = stripe.height;
end;

procedure stripesRandom(stripe:TStripe; brightness, count:byte);
var r:byte;
begin
    repeat
        r:=Random(stripe.height);
        linesBrightness[stripe.top+r]:=brightness;
        dec(count); 
    until count=0;
end;

procedure stripesGlitch(stripe:TStripe; brightness, count, len:byte);
var last:byte;
begin
    last:=linesBrightness[stripe.top];
    repeat
        stripesRandom(stripe, brightness, count);
        WaitFrame;
        setBrightnessRange(stripe, last);
        dec(len);
    until len=0;

end;

procedure randomizeBrightness(stripe:TStripe; brightness_start, brightness_end, frames:byte);
var i,b:byte;
begin
    
    while frames>0 do begin
        WaitFrames(fxDelay);
        b:=Random(byte(1+brightness_end-brightness_start))+brightness_start;
        for i:=stripe.top to stripe.bottom do linesBrightness[i] := b;
        dec(frames);
    end;
    for i:=stripe.top to stripe.bottom do linesBrightness[i] := brightness_end;
end;

procedure fadeAll(brightness_start, brightness_end:byte);
var i:byte;
begin
    if brightness_start<brightness_end then 
        for i:=brightness_start to brightness_end do brightenTo(i)
    else 
        for i:=brightness_start downto brightness_end do darkenTo(i);
end;

procedure fadeRange(stripe: TStripe; brightness_start, brightness_end:byte);
var i:byte;
begin
    if brightness_start<brightness_end then 
        for i:=brightness_start to brightness_end do setBrightnessRange(stripe, i)
    else 
        for i:=brightness_start downto brightness_end do setBrightnessRange(stripe, i);
end;

procedure shadeDown(stripe: TStripe; brightness, shadowSize: byte);
var bottom,l:byte;
    allLit:boolean;
    delta:shortint;
begin
    bottom:=stripe.top;
    delta:=1;
    if brightness<linesBrightness[stripe.top] then delta:=-1;
    repeat 
        allLit:=true;
        bottom := bottom + shadowSize;
        if bottom > stripe.bottom then bottom:=stripe.bottom;
        WaitFrames(fxDelay);
        for l:=stripe.top to bottom do 
            if linesBrightness[l]<>brightness then begin
                linesBrightness[l]:=linesBrightness[l]+delta;
                allLit:=false;
            end;
    until allLit;
end;

procedure shadeUp(stripe: TStripe; brightness, shadowSize: byte);
var bottom,l:byte;
    allLit:boolean;
    delta:shortint;
begin
    bottom:=stripe.bottom;
    delta:=1;
    if brightness<linesBrightness[stripe.top] then delta:=-1;
    repeat 
        allLit:=true;
        bottom := bottom - shadowSize;
        if bottom < stripe.top then bottom:=stripe.top;
        WaitFrames(fxDelay);
        for l:=bottom to stripe.bottom do 
            if linesBrightness[l]<>brightness then begin
                linesBrightness[l]:=linesBrightness[l]+delta;
                allLit:=false;
            end;
    until allLit;
end;

// ******************************* MAIN

begin
    poke($D208,1);
    poke($D20F,3);
    poke($D218,1);
    poke($D21F,3);
    dpoke($fe,SIREN);
    dpoke($fc,SIREN + 256);
    FillCHar(pointer(VIDEO_RAM_ADDRESS),40,127);

    msx.player := pointer(RMT_PLAYER_ADDRESS);
    msx.modul := pointer(RMT_MODULE_ADDRESS);
    xmem.create;
    for i:=15 downto 0 do begin
        if (color2 and 15) > i then color2 := (color2 and $f0) or i;
        if (color1 and 15) > i then color1 := (color1 and $f0) or i;
        pause(2);
    end;
    setBrightness(0);
    color2:=0;
    savmsc:=VIDEO_RAM_ADDRESS;
    
    for i:=0 to SCREEN_HEIGHT-1 do lineOffset[i]:=i*40;

    SystemOff;
    
    WaitFrame;
    Dpoke($D402,DISPLAY_LIST_ADDRESS);
    EnableVBLI(@VblSiren);
    EnableDLI(@dli);
    nmien := $c0;   
    fadeDelay:=4;

//    goto slide21;  // ******************* DEBUG JUMPER *************************************************************
    
    slide1: begin  // ************************** TGS logo
    
        fadeDelay:=3;
        fxDelay:=3;
        clearGFX;
        
        topStripe := MakeStripe(BMP_TGSlogo, BMP_TGSlogo_SIZE, 36);
        stripe1 := MakeStripe(BMP_TGStudio, BMP_TGStudio_SIZE, 96);
        stripe2 := MakeStripe(BMP_silly_venture, BMP_silly_venture_SIZE, 170);
        sirenptr :=0;
        repeat until sirenptr = $ff;
        LoadPic(stripe1);
        LoadPic(stripe2);
        LoadPic(topStripe);

        shadeUp(stripe1,15,2);
        fxDelay:=2;
        shadeDown(stripe1,9,1);
        fxDelay:=2;
        shadeUp(stripe1,15,4);
        sirenptr:=0;
        WaitFrames(50);
                
        fxDelay:=1;
        fadeDelay:=2;
        fadeRange(stripe2, 0, 15);
        fadeDelay:=3;
        fadeRange(stripe2, 15, 6);
        WaitFrames(50);
        
        randomizeBrightness(topStripe,0,8,30);
        randomizeBrightness(topStripe,0,15,30);

        WaitFrames(50);
        randomizeBrightness(topStripe,0,15,8);
        randomizeBrightness(topStripe,0,4,6);
        WaitFrames(20);
        randomizeBrightness(topStripe,0,12,10);
        WaitFrames(50);


        fadeDelay:=3;
        fadeAll(15,0);
    end;
 
    WaitFrame;
    msx.Init(0);  
    EnableVBLI(@vbl);
    nmien := $c0;   
            
    slide2: 
    
    slide3: begin  // ************************** welcome to komplex
    
        repeat until msx_line = 1;
        fadeDelay:=2;
        fxDelay:=1;
        clearGFX;
        topStripe := MakeStripe(BMP_betonowy_portal, BMP_betonowy_portal_SIZE, 20);
        stripe1 := MakeStripe(BMP_welcome_to_komplex, BMP_welcome_to_komplex_SIZE, 160);
        LoadPic(topStripe);
        
        repeat until msx_line = 0;
        fadeRange(topStripe, 0, 12);

        fxDelay:=2;
        LoadPic(stripe1);
        slideLeft(stripe1,15);
        repeat until msx_line = $6;
        randomizeBrightness(stripe1,0,15,8);

        repeat until msx_line = $8;
        randomizeBrightness(stripe1,0,15,8);

        repeat until msx_line = $C;

        slideUpDown(topStripe, 0, 6);
        slideUpDown(stripe1, 0, 6);
    end;    

    slide4: begin  // ************************** You are not an individual being anymore.
        
        clearGFX;
        fadeDelay:=1;
        fxDelay:=1;
        topStripe := MakeStripe(BMP_pochod, BMP_pochod_SIZE, 20);
        stripe1 := MakeStripe(BMP_you_are_not, BMP_you_are_not_SIZE, 160);
        
        LoadPic(topStripe);
        
        repeat until msx_line = $0;
        fadeRange(topStripe,0,12);

        LoadPic(stripe1);
        slideDown(stripe1,15,1);

        repeat until msx_line = $8;
        randomizeBrightness(stripe1,0,15,8);

        repeat until msx_line = $e;
        slideDown(topStripe,0,8);
        slideDown(stripe1,0,4);
    end;

    slide5: begin  // ************************** You are just a number in a queue.

        topStripe := MakeStripe(BMP_nadzorca1, BMP_nadzorca1_SIZE, 20);
        stripe1 := MakeStripe(BMP_you_are_just, BMP_you_are_just_SIZE, 160);

        LoadPic(topStripe);
        shadeUp(topStripe,14,8);

        LoadPic(stripe1);
        stripeIn(stripe1,12,6);
 
        repeat until msx_line = $8;
        stripesGlitch(topStripe, 0, 4, 10);
        repeat until msx_line = $C;
        randomizeBrightness(stripe1,0,15,8);

        repeat until msx_line = $0;
        stripeOut(stripe1);
        fxDelay:=1;
        shadeDown(topStripe,0,3);
    end;    

    slide6: begin  // ************************** Citizen 21375-TY4, prepare yourself

        fadeDelay:=1;
        topStripe := MakeStripe(BMP_obywatelka2, BMP_obywatelka2_SIZE, 20);
        stripe1 := MakeStripe(BMP_citizen_213, BMP_citizen_213_SIZE, 160);
        
        LoadPic(topStripe);
        LoadPic(stripe1);

        repeat until msx_line = $0;
        randomizeBrightness(stripe1,0,15,8);
        fadeRange(topStripe,0,12);

        i:=1;
        repeat
            WaitFrames(1);
            randomizeBrightness(stripe1,0,12,8);
            inc(i);
            repeat until msx_line = i;
            inc(i);
            WaitFrames(1);
            repeat 
                interlaceRange(topStripe,10,8 - i shr 1);
                interlaceRange(topStripe,10,10 - i shr 1);
            until msx_line = i;
        until i=$b;

        fadeAll(15,0);
    end;    

    slide7: begin  // ************************** to be constantly examined and rated.

        topStripe := MakeStripe(BMP_nadzorca2, BMP_nadzorca2_SIZE, 20);
        stripe1 := MakeStripe(BMP_to_be_constantly, BMP_to_be_constantly_SIZE, 160);        
        
        LoadPic(topStripe);
        LoadPic(stripe1);

        repeat until msx_line = $0;
        shadeUp(topStripe,14,16);
        randomizeBrightness(stripe1,0,8,8);
        randomizeBrightness(stripe1,0,15,8);
                
        repeat until msx_line = $6; 
        stripesGlitch(topStripe,8,2,10);
        repeat until msx_line = $8; 
        stripesGlitch(topStripe,0,5,10);

        repeat until msx_line = $c; 
        randomizeBrightness(stripe1,0,15,8);
        stripesGlitch(topStripe,0,5,5);

        fadeAll(15,0);
    end;    

    slide8: begin  // ************************** Judged useful or to be disposed by the machines.

        topStripe := MakeStripe(BMP_automat_biletowy, BMP_automat_biletowy_SIZE, 20);
        stripe1 := MakeStripe(BMP_judged_useful, BMP_judged_useful_SIZE, 148);        
        stripe2 := MakeStripe(BMP_by_the_machines, BMP_by_the_machines_SIZE, 168);        

        LoadPic(topStripe);
        shadeDown(topStripe,14,6);
        
        LoadPic(stripe1);
        slideLeft(stripe1,14);
        randomizeBrightness(stripe1,0,8,8);
        randomizeBrightness(stripe1,0,15,8);


        repeat until msx_line = $f; 
        fadeDelay:=4;
        LoadPic(stripe2);
        fadeRange(stripe2,0,14);

        repeat until msx_line = $8; 
        shadeDown(topStripe,0,3);
        shadeDown(stripe1,0,3);


        repeat until msx_line = $d; 
        randomizeBrightness(stripe2,0,15,16);
        fadeDelay:=2;
        fadeAll(15,0);
    end;    

    slide9: 

    slide10: begin  // ************************** in the world where no one cares
        
        fadeDelay:=1;
        clearGFX;
        stripe1 := MakeStripe(BMP_in_the_world, BMP_in_the_world_SIZE, 90);        
        LoadPic(stripe1);

        repeat until msx_line = $8; 
        slideLeft(stripe1,12);
    end;    

    slide11: begin  // ************************** will you care?
        
        stripe1 := MakeStripe(BMP_will_you_care, BMP_will_you_care_SIZE, 90);        

        repeat until msx_line = $2; 
        LoadPic(stripe1);
        setBrightnessRange(stripe1,15);
        WaitFrames(4);
        setBrightnessRange(stripe1,8);
        repeat until msx_line = $3; 
        setBrightnessRange(stripe1,15);
        WaitFrames(4);
        setBrightnessRange(stripe1,8);
        WaitFrames(4);
        randomizeBrightness(stripe1,0,12,16);
        
        repeat until msx_line = $6; 
        setBrightnessRange(stripe1,15);
        WaitFrames(6);
        randomizeBrightness(stripe1,14,0,16);
    end;    

    slide12:
        setBrightness(0);

    slide13: begin  // ************************** GET BACK TO YOUR ROW!

        fadeDelay:=1;
        topStripe := MakeStripe(BMP_przydzial, BMP_przydzial_SIZE, 20);
        stripe1 := MakeStripe(BMP_get_back_to, BMP_get_back_to_SIZE, 160);        
        
        LoadPic(topStripe);
        LoadPic(stripe1);
        repeat until msx_line = $B; 
        setBrightnessRange(topStripe,15);
        randomizeBrightness(stripe1,0,8,16);
        setBrightnessRange(topStripe,12);
        randomizeBrightness(stripe1,0,15,8);
                
        stripesGlitch(topStripe,8,2,10);
        WaitFrames(4);
        stripesGlitch(topStripe,0,5,10);
        setBrightnessRange(topStripe,15);
        
        repeat until msx_line = $0; 
        slideUpDown(topStripe,0,16);
        randomizeBrightness(stripe1,12,0,8);        
    end;    

    slide14: begin  // ************************** AND SERVE

        topStripe := MakeStripe(BMP_kombinat, BMP_kombinat_SIZE, 20);
        stripe1 := MakeStripe(BMP_and_serve, BMP_and_serve_SIZE, 160);        
        LoadPic(topStripe);
        LoadPic(stripe1);
        repeat until msx_line = $3; 
        setBrightnessRange(topStripe,15);
        randomizeBrightness(stripe1,0,8,16);
        setBrightnessRange(topStripe,12);
        randomizeBrightness(stripe1,0,15,8);
                
        stripesGlitch(topStripe,8,2,10);
        WaitFrames(4);
        stripesGlitch(topStripe,0,5,10);
        setBrightnessRange(topStripe,15);

        repeat until msx_line = $8; 
        slideUpDown(topStripe,0,16);
        randomizeBrightness(stripe1,12,0,8);
    end;   

    slide15: begin  // ************************** PERFORM

        topStripe := MakeStripe(BMP_instytut, BMP_instytut_SIZE, 20);
        stripe1 := MakeStripe(BMP_perform, BMP_perform_SIZE, 160);        
        LoadPic(topStripe);
        LoadPic(stripe1);
        repeat until msx_line = $B; 
        setBrightnessRange(topStripe,15);
        randomizeBrightness(stripe1,0,8,16);
        setBrightnessRange(topStripe,12);
        randomizeBrightness(stripe1,0,15,8);
                
        stripesGlitch(topStripe,8,2,10);
        WaitFrames(4);
        stripesGlitch(topStripe,0,5,10);
        setBrightnessRange(topStripe,15);

        repeat until msx_line = $0; 
        slideUpDown(topStripe,0,16);
        randomizeBrightness(stripe1,12,0,8);
    end;   

    slide16: begin  // ************************** INFORM

        topStripe := MakeStripe(BMP_agent, BMP_agent_SIZE, 20);
        stripe1 := MakeStripe(BMP_inform, BMP_inform_SIZE, 160);        
        LoadPic(topStripe);
        LoadPic(stripe1);
        repeat until msx_line = $3; 
        setBrightnessRange(topStripe,15);
        randomizeBrightness(stripe1,0,8,16);
        setBrightnessRange(topStripe,12);
        randomizeBrightness(stripe1,0,15,8);
                
        repeat until msx_line = $6; 
        randomizeBrightness(topStripe,5,12,16);

        i:=10;
        repeat 
            repeat until msx_line = (i and 15); 
            WaitFrames(8);
            stripesGlitch(topStripe,18-i,i,8);
            inc(i,4);
        until i = 22;

        repeat until msx_line = $3; 
        randomizeBrightness(stripe1,15,0,16);
        slideUpDown(topStripe,0,10);
    end;  

    slide17: // pause

    slide18: begin  // ************************** there is no point to resist
        
        stripe1 := MakeStripe(BMP_there_is_resist, BMP_there_is_resist_SIZE, 102);        
        stripe2 := MakeStripe(BMP_there_is_exist, BMP_there_is_exist_SIZE, 102);        

        BuffPic(stripe2,640);
        BuffPic(stripe1,0);
        LoadBuf(stripe1,0);
        slideLeft(stripe1,14);
        repeat until msx_line = $8; 
        stripesGlitch(stripe1,0,5,10);
        randomizeBrightness(stripe1,0,10,8);
    end;    

    slide19: begin  // ************************** exist

        repeat until msx_line = $A; 
        repeat 
            setBrightnessRange(stripe2,random(12));
            LoadBuf(stripe1,0);
            setBrightnessRange(stripe1,random(12));
            LoadBuf(stripe2,640);
            waitFrames(random(4)+1);
        until msx_line = $D;    
        setBrightnessRange(stripe1,14);

        repeat until msx_line = $0; 
        randomizeBrightness(stripe1,0,12,16);
        
        repeat until msx_line = $2; 
        fadeRange(stripe1,12,0);
    end;    

    slide20: 

    slide21: begin  // ************************** Citizen 21375-TY4, prepare yourself.

        topStripe := MakeStripe(BMP_widok, BMP_widok_SIZE, 20);
        stripe1 := MakeStripe(BMP_citizen_213_, BMP_citizen_213__SIZE, 160);        
        LoadPic(topStripe);
        LoadPic(stripe1);
        
        repeat until msx_line = $5; 
        WaitFrames(24);
        poke(710,12);
        WaitFrames(12);
        poke(710,0);
        randomizeBrightness(topStripe,0,12,8);
        poke(710,12);
        WaitFrames(12);
        poke(710,0);
        randomizeBrightness(stripe1,0,12,8);
        randomizeBrightness(topStripe,6,12,24);

        repeat until msx_line = $B; 
        stripesGlitch(topStripe,8,2,10);
        pause(10);
        stripesGlitch(topStripe,0,5,10);
        pause(10);

        repeat until msx_line = $F; 
        fadeAll(15,0);
    end; 

    slide22: begin  // ************************** tower swipe

        clearGFX;
        fxDelay:=2;
        fadeDelay:=3;
        WaitFrame;
        nmien := $40;     
        poke(709,0);
        Dpoke($D402,DISPLAY_LIST_ADDRESS2);
        xmem.Position := BMP_tower0;
        xmem.ReadBuffer(vmem, BMP_tower0_SIZE);
        repeat until msx_line = $2; 
        for i:=0 to 4 do begin
            poke(709,i);
            WaitFrames(fadeDelay);
        end;
        
        xmem.Position := BMP_tower1;
        xmem.ReadBuffer(vmem[$1000], BMP_tower1_SIZE);
        WaitFrames(fxDelay);
        Dpoke(DISPLAY_LIST_ADDRESS2+7,VIDEO_RAM_ADDRESS+$1000);
        poke(709,6);
        
        xmem.Position := BMP_tower2;
        xmem.ReadBuffer(vmem, BMP_tower2_SIZE);
        WaitFrames(fxDelay);
        Dpoke(DISPLAY_LIST_ADDRESS2+7,VIDEO_RAM_ADDRESS);
        poke(709,8);

        xmem.Position := BMP_tower3;
        xmem.ReadBuffer(vmem[$1000], BMP_tower3_SIZE);
        WaitFrames(fxDelay);
        Dpoke(DISPLAY_LIST_ADDRESS2+7,VIDEO_RAM_ADDRESS+$1000);
        poke(709,10);
        poke(710,2);
        poke(712,2);

        xmem.Position := BMP_tower4;
        xmem.ReadBuffer(vmem, BMP_tower4_SIZE);
        WaitFrames(fxDelay);
        Dpoke(DISPLAY_LIST_ADDRESS2+7,VIDEO_RAM_ADDRESS);
        poke(709,12);
        poke(710,4);
        poke(712,4);

        xmem.Position := BMP_tower5;
        xmem.ReadBuffer(vmem[$1000], BMP_tower5_SIZE);
        WaitFrames(fxDelay);
        Dpoke(DISPLAY_LIST_ADDRESS2+7,VIDEO_RAM_ADDRESS+$1000);
        poke(709,14);
        poke(710,6);
        poke(712,6);

        xmem.Position := BMP_tower6;
        xmem.ReadBuffer(vmem, BMP_tower6_SIZE);
        WaitFrames(fxDelay);
        Dpoke(DISPLAY_LIST_ADDRESS2+7,VIDEO_RAM_ADDRESS);
        poke(709,14);
        poke(710,8);
        poke(712,8);
        
        xmem.Position := BMP_tower7;
        xmem.ReadBuffer(vmem[$1000], BMP_tower7_SIZE);
        WaitFrames(fxDelay);
        Dpoke(DISPLAY_LIST_ADDRESS2+7,VIDEO_RAM_ADDRESS+$1000);
        poke(709,14);
        poke(710,10);
        poke(712,10);

        xmem.Position := BMP_tower8;
        xmem.ReadBuffer(vmem, BMP_tower8_SIZE);
        WaitFrames(fxDelay);
        Dpoke(DISPLAY_LIST_ADDRESS2+7,VIDEO_RAM_ADDRESS);
        poke(709,14);
        poke(710,12);
        poke(712,12);

        WaitFrames(fxDelay);
        poke(710,14);
        poke(712,14);

        setBrightness(14);
        WaitFrame;
        nmien := $c0;    
        Dpoke($D402,DISPLAY_LIST_ADDRESS); 
    end; 

    slide23: begin  // ************************** title screen

        fxDelay:=1;
        topStripe := MakeStripe(BMP_titlescreen, BMP_titlescreen_SIZE, 0);
        LoadPic(topStripe);

        for i:=14 downto 0 do begin
            poke(710,i);
            poke(712,i);
            WaitFrames(fadeDelay);
        end;

        fadeDelay:=1;
        repeat until msx_line = $4; 
        fadeAll(15,0);
    end; 

    slide24: begin  // ************************** adverts

        setBrightness(0);
        
        topStripe := MakeStripe(BMP_over_100_unique, BMP_over_100_unique_SIZE, 10);
        LoadPic(topStripe);
        repeat until msx_line = $7; 
        slideLeft(topStripe,14);

        topStripe := MakeStripe(BMP_responsive_env, BMP_responsive_env_SIZE, 52);
        LoadPic(topStripe);
        repeat until msx_line = $B; 
        slideLeft(topStripe,14);

        fxDelay:=2;
        topStripe := MakeStripe(BMP_more_than_10, BMP_more_than_10_SIZE, 98);
        LoadPic(topStripe);
        repeat until msx_line = $F; 
        slideLeft(topStripe,14);

        topStripe := MakeStripe(BMP_amazing_soundtrack, BMP_amazing_soundtrack_SIZE, 128);
        LoadPic(topStripe);
        repeat until msx_line = $3; 
        slideLeft(topStripe,14);

        fxDelay:=1;
        topStripe := MakeStripe(BMP_immersive_and, BMP_immersive_and_SIZE, 162);
        LoadPic(topStripe);
        repeat until msx_line = $7; 
        slideLeft(topStripe,14);

        repeat until msx_line = $E; 
        fadeAll(15,0);
    end; 

    slide25: begin  // ************************** future you
        
        setBrightness(0);
        topStripe := MakeStripe(BMP_this_is_the, BMP_this_is_the_SIZE, 40);
        LoadPic(topStripe);
        repeat until msx_line = $0; 
        shadeDown(topStripe,14,6);
        topStripe := MakeStripe(BMP_i_have_ever, BMP_i_have_ever_SIZE, 60);
        LoadPic(topStripe);
        repeat until msx_line = $2; 
        shadeDown(topStripe,14,6);
        topStripe := MakeStripe(BMP_future_you, BMP_future_you_SIZE, 90);
        LoadPic(topStripe);
        repeat until msx_line = $7; 
        for i:=1 to 9 do begin 
            WaitFrames(12);
            randomizeBrightness(topStripe,random(10),12,12);
        end;

        repeat until msx_line = $4; 
        fadeAll(15,0);
    end; 

    slide26: begin  // ************************** coming 2020

        WaitFrames(50);
        poke($D208,0);
        poke($D20F,3);
        poke($D218,0);
        poke($D21F,3);
        //msx.Stop;
        EnableVBLI(@VblSiren);
        nmien := $c0; 

        topStripe := MakeStripe(BMP_titlescreen, BMP_titlescreen_SIZE, 0);
        LoadPic(topStripe);
        shadeDown(topStripe,14,6);
        sirenptr:=0;
        repeat until sirenptr = $ff;
        WaitFrames(100);
        sirenptr:=0;
        stripe2 := MakeStripe(BMP_coming2020, BMP_coming2020_SIZE-1, 16);
        LoadPic(stripe2);
        repeat until sirenptr = $ff;
        WaitFrames(200);
        slideUpDown(topStripe,0,4);
    end; 

    repeat until false; // inf loop

end.
