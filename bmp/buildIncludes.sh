#!/bin/bash
EXT=gr8
FILES=$(ls -1 *.$EXT)
FILECOUNT=$(ls -1 *.$EXT | wc -l)
PREFIX='BMP_'
BPATH='bmp/'
POINTER=0
rm bitmaps.rc
rm bitmaps.inc

echo "const" > bitmaps.inc
echo "; resource loading file" > bitmaps.rc

for IMAGE in $FILES
do
    echo \*\*\* Adding $IMAGE
    NAME="${IMAGE%.*}"
    SIZE=$(stat -c%s "$IMAGE")
    echo "  ${PREFIX}${NAME} = $POINTER ;" >> bitmaps.inc
    echo "  ${PREFIX}${NAME}_SIZE = $SIZE ;" >> bitmaps.inc
    echo "${PREFIX}${NAME} extmem '${BPATH}${IMAGE}'" >> bitmaps.rc
    POINTER=$(($POINTER+$SIZE))
done

