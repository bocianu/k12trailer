//
// ;*** define your project wide constants here
// 
// ;*** I like to keep memory locations at top of this file
//

FREE_BOTTOM = $3000;  // and sometimes I like to do some relative addressing
VIDEO_RAM_ADDRESS = FREE_BOTTOM;
BUFFER = VIDEO_RAM_ADDRESS + $2000; // 5000
GFX_MEM = VIDEO_RAM_ADDRESS + 96;
DISPLAY_LIST_ADDRESS = BUFFER + $1000; // $6000
DISPLAY_LIST_ADDRESS2 = DISPLAY_LIST_ADDRESS + $100; // $6100
SIREN = $6400;

RMT_PLAYER_ADDRESS = $2400;
RMT_MODULE_ADDRESS = $E000;

// ;*** and here goes all other stuff

SCREEN_HEIGHT = 200;
